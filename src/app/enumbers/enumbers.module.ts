import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ENumbersPageRoutingModule } from './enumbers-routing.module';

import { ENumbersPage } from './enumbers.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ENumbersPageRoutingModule
  ],
  declarations: [ENumbersPage]
})
export class ENumbersPageModule {}
