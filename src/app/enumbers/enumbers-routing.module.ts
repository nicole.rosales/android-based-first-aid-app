import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ENumbersPage } from './enumbers.page';

const routes: Routes = [
  {
    path: '',
    component: ENumbersPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ENumbersPageRoutingModule {}
