import { Component } from '@angular/core';
import { ActionSheetController } from '@ionic/angular';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  constructor(public actionSheet: ActionSheetController) { }

  async presentActionSheet() {
    const actionSheet = await this.actionSheet.create({
      header: 'Are you sure to take the First Aid Quiz?',
      buttons: [
        {
          text: 'Yes',
          role: 'Yes',
          icon: 'open',
          handler: () => {
            console.log('Cancel');
          }
        },
        {
          text: 'No',
          role: 'cancel',
          icon: 'close',
          handler: () => {
            console.log('sa');
          }
        }   
      ]

    });
    await actionSheet.present();
  }

}
